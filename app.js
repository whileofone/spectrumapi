const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const user = require('./user');
const category = require('./category');
const subCategory = require('./subCategory');
const product = require('./product');
const shop = require('./shop');
const brand = require('./brand');
const order = require('./order');

const app = express();

app.use(bodyParser.json());
app.use(fileUpload());

app.get('/', (req, res) => {
  res.send('Hey There...!');
});
app.use(express.static('uploads'));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');
    next();
});

app.post('/uploadImage', (req, res) => {
  if (!req.files) {
    return res.status(400).send('No files were uploaded.');
  }
 
  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  const sampleFile = req.files.productImage;
 
  // Use the mv() method to place the file somewhere on your server
  const timestamp = new Date().getTime().toString();
  const uploadImageName = sampleFile.name;
  const ext = uploadImageName.substr(uploadImageName.lastIndexOf('.'), uploadImageName.length);
  
  const imageToUpload = `${__dirname}/uploads/Product/${timestamp}${ext}`; 
  sampleFile.mv(imageToUpload, (err) => {
    if (err) { 
      return res.status(500).send(err); 
    }
 
    res.send(`Product/${timestamp}${ext}`);
  });
});

/******** User model API ***************/
app.post('/AdminLogin', (req, res) => {
  user.AdminLogin(req.body, res);
});

app.get('/GetAllUser', (req, res) => {
  user.SelectAllUser(req, res);
});

app.post('/InsertUser', (req, res) => {
  user.insertUser(req.body, res);
});

app.get('/GetUser/:id', (req, res) => {
  user.SelectUser(req, res);
});

app.post('/UpdateUser', (req, res) => {
  user.updateUser(req.body, res);
});

app.post('/GenerateOTP', (req, res) => {
  user.generateotp(req.body, res);
});

app.post('/CreateUserApp', (req, res) => {
  user.createuserapp(req.body, res);
});

app.post('/UpdateUserApp', (req, res) => {
  user.updateuserapp(req.body, res);
});

app.post('/UpdatePassword', (req, res) => {
  user.updatepassword(req.body, res);
});

app.post('/CheckUserExist', (req, res) => {
  user.checkuserexist(req.body, res);
});

app.post('/LoginUserApp', (req, res) => {
  user.loginUserApp(req.body, res);
});
/******** User model API ***************/

/*********** Category Model API *************/
app.get('/GetAllCategotry', (req, res) => {
  category.SelectAllCategory(req, res);
});

app.post('/InsertCategory', (req, res) => {
  category.InsertCategory(req.body, res);
});

app.get('/GetCategory/:id', (req, res) => {
  category.SelectCategory(req, res);
});

app.post('/UpdateCategory', (req, res) => {
  category.UpdateCategory(req.body, res);
});

app.post('/DeleteCategoryDetails/:id', (req, res) => {
  category.DeleteCategory(req, res);
}); 

/*********** Category Model API *************/

/************* subCategory Model API **********/

app.get('/GetAllSubCategory', (req, res) => {
  subCategory.SelectAllSubCategory(req, res);
});

app.post('/InsertSubCategory', (req, res) => {
  subCategory.InsertSubCategory(req.body, res);
});

app.post('/UpdateSubCategory', (req, res) => {
  subCategory.UpdateSubCategory(req.body, res);
});

app.get('/GetSubCategory/:id', (req, res) => {
  subCategory.SelectSubCategory(req, res);
});

app.post('/DeleteSubCategory', (req, res) => {
  subCategory.DeleteSubCategory(req.body, res);
});

app.get('/GetSubCategoryByCategotry/:categotryId', (req, res) => {
  subCategory.AllSubCategoryByCategotry(req, res);
});

/************* subCategory Model API **********/

/************** Product Model API  ************/

app.get('/GetAllProduct', (req, res) => {
  product.SelectAllProduct(req, res);
});

app.post('/InsertProduct', (req, res) => {
  product.insertProduct(req.body, res);
});

app.post('/UpdateProduct', (req, res) => {
  product.updateProduct(req.body, res);
});

app.post('/DeleteProductDetails/:id', (req, res) => {
  product.DeleteProduct(req, res);
});

app.get('/GetProductDetails/:id', (req, res) => {
  product.SelectProduct(req, res);
});

app.post('/GetProductBySequence', (req, res) => {
  product.GetProductBySequence(req.body, res);
});

app.get('/GetProductBasedOnCategory/:categotryId/:subCategotryId', (req, res) => {
  product.GetProductBasedOnCategory(req, res);
});

app.get('/GetRandomProduct', (req, res) => {
  product.GetRandomProduct(req, res);
});

app.get('/GetProductInfoByProductID/:productId', (req, res) => {
  product.GetProductInfoByProductID(req, res);
});
/************** Product Model API  ************/

/************** Shop Model API  ************/

app.get('/GetAllShop', (req, res) => {
  shop.SelectAllShop(req, res);
});


app.post('/InsertShop', (req, res) => {
  shop.insertShop(req.body, res);
});


app.post('/UpdateShop', (req, res) => {
  shop.updateShop(req.body, res);
});

app.get('/GetShop/:id', (req, res) => {
  shop.SelectShop(req, res);
});

/************** Shop Model API  ************/

/************** Brand Model API  ************/

app.get('/GetAllBrand', (req, res) => {
   brand.SelectAllBrand(req, res);
});

app.post('/InsertBrand', (req, res) => {
  brand.insertBrand(req.body, res);
});

app.post('/UpdateBrand', (req, res) => {
  brand.updateBrand(req.body, res);
});

app.post('/DeleteBrandDetails/:id', (req, res) => {
  brand.DeleteBrand(req, res);
});

app.get('/GetBrandDetails/:id', (req, res) => {
  brand.SelectBrand(req, res);
});

app.get('/GetBrandBySequenceId/:categotryId/:subCategotryId', (req, res) => {
  brand.GetBrandBySequence(req, res);
});

/************** Brand Model API  ************/

// /**************** Order Model API ************/
app.get('/GetAllOrders', (req, res) => {
  order.SelectAllOrders(req, res);
});

app.get('/GetOrderForUser/:UserID', (req, res) => {
  order.GetOrderForUser(req, res);
});

app.post('/InsertOrder', (req, res) => {
  order.insertOrder(req.body, res);
});

app.post('/CheckoutOrderInsert', (req, res) => {
  order.CheckoutOrderInsert(req.body, res);
});

app.listen(3000);
