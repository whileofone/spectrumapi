const connection = require('./const').connection;

let FinalResult = '';


const SelectAllBrand = (req, res) => {
    
    connection.query('SELECT tblB.brandID as BrandID,tblB.brandName as BrandName,tblB.categoryID, tblC.category_name as CategoryName, tblSC.subCategory_name as SubCategoryName from tblBrand tblB INNER JOIN tblCategory tblC ON tblB.categoryID=tblC.category_id INNER JOIN tblSubCategory tblSC ON tblB.subCategoryID=tblSC.subCategoryID WHERE isActive=1', (error, rows) => {
        if (!error) {
                FinalResult = { status: 'success', record_count: rows.length, records: rows };
                res.json(FinalResult);
            } else {
                FinalResult = { status: 'Failure', Msg: 'Error in fetching Brand' };
                res.json(FinalResult);
            }
});
};


const insertBrand = (insertJSON, res) => {
    const query = `INSERT INTO tblBrand(brandName,categoryID,subCategoryID) values('${insertJSON.BrandName}','${insertJSON.CategoryID}','${insertJSON.SubCategoryID}')`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Insertion Failed' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Success', messege: 'Inserted Successfully' };
            res.json(FinalResult);
        }
    });
};


const updateBrand = (updateJSON, res) => {
    const query = `UPDATE tblBrand SET brandName='${updateJSON.BrandName}',categoryID='${updateJSON.CategoryID}',subCategoryID='${updateJSON.SubCategoryID}' WHERE 
    brandID='${updateJSON.BrandID}'`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Insertion Failed' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Success', messege: 'Updated Successfully', query : query };
            res.json(FinalResult);
        }
    });
};

const DeleteBrand = (req, res) => {
    const query = `UPDATE tblBrand SET isActive=0 WHERE brandID='${req.params.id}'`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Failed to delete' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Success', messege: 'Brand Deleted Successfully' };
            res.json(FinalResult);
        }
    });
};

const SelectBrand = (req, res) => {
    const query = `SELECT tblB.brandID as BrandID,tblB.brandName as BrandName,tblB.categoryID AS CategoryID,tblB.subCategoryID AS SubCategoryID, tblC.category_name as CategoryName, tblSC.subCategory_name as SubCategoryName FROM tblBrand tblB INNER JOIN tblcategory tblC ON tblB.categoryID=tblC.category_id INNER JOIN tblSubCategory tblSC ON tblB.subCategoryID=tblSC.subCategoryID WHERE brandID=${req.params.id} AND isActive=1`;
    connection.query(query, (error, rows) => {
        if (!error) {
            FinalResult = { status: 'success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching Brand' };
            res.json(FinalResult);
        }
    });
};  

const GetBrandBySequence = (req, res) => {
    const query = `SELECT tblB.brandID as BrandID,tblB.brandName as BrandName,tblB.categoryID AS CategoryID,tblB.subCategoryID AS SubCategoryID, tblC.category_name as CategoryName, tblSC.subCategory_name as SubCategoryName FROM tblBrand tblB INNER JOIN tblcategory tblC ON tblB.categoryID=tblC.category_id INNER JOIN tblSubCategory tblSC ON tblB.subCategoryID=tblSC.subCategoryID WHERE tblC.category_id=${req.params.categotryId} AND tblSC.subCategoryID=${req.params.subCategotryId}`;
    connection.query(query, (error, rows) => {
        if (!error) {
            FinalResult = { status: 'success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching Brand'};
            res.json(FinalResult);
        }
    });
};  


module.exports.SelectAllBrand = SelectAllBrand;
module.exports.insertBrand = insertBrand;
module.exports.updateBrand = updateBrand;
module.exports.DeleteBrand = DeleteBrand;
module.exports.SelectBrand = SelectBrand;
module.exports.GetBrandBySequence = GetBrandBySequence;
