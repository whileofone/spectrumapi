const connection = require('./const').connection;

   let FinalResult = '';

const SelectAllCategory = (req, res) => {
    connection.query('SELECT category_id AS CategoryID,category_name AS CategoryName,shop FROM tblCategory WHERE IsView = 1', (error, rows) => {
        if (!error) {
            FinalResult = { status: 'success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching category' };
            res.json(FinalResult);
        }
    });    
};

const InsertCategory = (insertJSON, res) => {
    const query = `INSERT INTO tblCategory(category_name) VALUES ('${insertJSON.categoryName}')`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Insertion Failed' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'success', messege: 'Inserted Successfully' };
            res.json(FinalResult);
        }
    });
};

const UpdateCategory = (updateJSON, res) => {
    const query = `UPDATE tblCategory SET category_name='${updateJSON.categoryName}' WHERE category_id='${updateJSON.categoryID}'`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Update Failed' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'success', messege: 'Updated Successfully' };
            res.json(FinalResult);
        }
    });
};

const DeleteCategory = (req, res) => {
    const query = `UPDATE tblCategory SET IsView=0 WHERE category_id='${req.params.id}'`;
    connection.query(query, (error) => {
        if (error) {
            FinalResult = { status: 'failure', messege: 'Failed to delete' };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Success', messege: 'Category Deleted Successfully', query : query };
            res.json(FinalResult);
        }
    });
};

const SelectCategory = (req, res) => {
    const categoryID = req.params.id;
    connection.query('SELECT category_id, category_name, shop FROM tblCategory WHERE category_id= ?', [categoryID], (error, rows) => {
        if (!error) {
            FinalResult = { status: 'success', record_count: rows.length, records: rows };
            res.json(FinalResult);
        } else {
            FinalResult = { status: 'Failure', Msg: 'Error in fetching category' };
            res.json(FinalResult);
        }
    });
};  
module.exports.SelectAllCategory = SelectAllCategory;
module.exports.InsertCategory = InsertCategory;
module.exports.UpdateCategory = UpdateCategory;
module.exports.DeleteCategory = DeleteCategory;
module.exports.SelectCategory = SelectCategory;


